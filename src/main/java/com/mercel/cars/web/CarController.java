package com.mercel.cars.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mercel.cars.model.Car;
import com.mercel.cars.repository.CarRepository;

@RestController
@RequestMapping(path = "/cars")
public class CarController {
	@Autowired
	private CarRepository repository;
	
	@GetMapping
	public Iterable<Car> all() {
		return repository.findAll();
	}
	
	@GetMapping("/{id}")
	public Car get(@PathVariable Integer id) {
		return repository.findById(id).get();
	}
	
	@PostMapping
	public Car create(@RequestBody Car car) {
		return repository.save(car);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Integer id) {
		repository.deleteById(id);
	}
}
